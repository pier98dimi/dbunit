package ju4;

import dao.EmployeeDao;
import dao.EmployeeDaoImpl;
import factory.HibernateSessionFactory;
import java.io.FileInputStream;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.Session;
import org.junit.Assert;

public class EmployeeDaoImplTest extends DBTestCase {

  private EmployeeDao employeeDao;

  public EmployeeDaoImplTest(String name) {
    super(name);
    System.setProperty(
        PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, "org.postgresql.Driver");
    System.setProperty(
        PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL,
        "jdbc:postgresql://localhost:5432/postgres");
    System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, "postgres");
    System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, "");
  }

  @Override
  protected IDataSet getDataSet() throws Exception {
    String file =
        "/Users/pier/Documents/unimi/verifica e convalida software/progetto_DbUnit/src/test/resources/actualDataset.xml";
    return new FlatXmlDataSetBuilder().build(new FileInputStream(file));
  }

  public void testGetAverageAge() {
    Session session = HibernateSessionFactory.getSession();
    employeeDao = new EmployeeDaoImpl(session);
    Long avgAge = employeeDao.getAverageAge();
    session.close();

    Assert.assertEquals(avgAge, new Long(30));
  }

  protected DatabaseOperation getSetUpOperation() {
    return DatabaseOperation.CLEAN_INSERT;
  }

  protected DatabaseOperation getTearDownOperation() {
    return DatabaseOperation.NONE;
    // return DatabaseOperation.DELETE;
  }
}
