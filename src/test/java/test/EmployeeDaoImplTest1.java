package test;

import dao.EmployeeDao;
import dao.EmployeeDaoImpl;
import factory.HibernateSessionFactory;
import java.io.FileInputStream;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EmployeeDaoImplTest1 {
  private static final String JDBC_DRIVER = org.postgresql.Driver.class.getName();
  private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/postgres";
  private static final String USER = "postgres";
  private static final String PASSWORD = "";
  private static IDatabaseTester databaseTester;

  private EmployeeDao employeeDao;

  @BeforeEach
  public void setUp() throws Exception {
    cleanInsert(readDataSet());
  }

  private void cleanInsert(IDataSet dataSet) throws Exception {
    databaseTester = new JdbcDatabaseTester(JDBC_DRIVER, JDBC_URL, USER, PASSWORD);
    databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
    databaseTester.setDataSet(dataSet);
    databaseTester.onSetup();
  }

  private IDataSet readDataSet() throws Exception {
    String file =
        "/Users/pier/Documents/unimi/verifica e convalida software/progetto_DbUnit/src/test/resources/actualDataset.xml";
    return new FlatXmlDataSetBuilder().build(new FileInputStream(file));
  }

  @Test
  public void testGetAverageAge() {
    Session session = HibernateSessionFactory.getSession();
    employeeDao = new EmployeeDaoImpl(session);
    Long avgAge = employeeDao.getAverageAge();
    session.close();

    Assert.assertEquals(avgAge, new Long(30));
  }

  @AfterEach
  public void tearDown() throws Exception {
    databaseTester.setTearDownOperation(DatabaseOperation.NONE);
    // databaseTester.setTearDownOperation(DatabaseOperation.DELETE);
    databaseTester.onTearDown();
  }
}
