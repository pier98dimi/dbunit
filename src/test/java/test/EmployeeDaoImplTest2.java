package test;

import dao.EmployeeDao;
import dao.EmployeeDaoImpl;
import factory.HibernateSessionFactory;
import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EmployeeDaoImplTest2 {
  private static final String JDBC_DRIVER = org.postgresql.Driver.class.getName();
  private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/postgres";
  private static final String USER = "postgres";
  private static final String PASSWORD = "";
  private static IDatabaseTester databaseTester;

  private EmployeeDao employeeDao;

  @BeforeEach
  public void setUp() throws Exception {
    cleanInsert(readDataSet());
  }

  private void cleanInsert(IDataSet dataSet) throws Exception {
    databaseTester = new JdbcDatabaseTester(JDBC_DRIVER, JDBC_URL, USER, PASSWORD);
    databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
    databaseTester.setDataSet(dataSet);
    databaseTester.onSetup();
  }

  private IDataSet readDataSet() throws MalformedURLException, DataSetException {
    String file =
        "/Users/pier/Documents/unimi/verifica e convalida software/progetto_DbUnit/src/test/resources/actualDataset.xml";
    return new FlatXmlDataSetBuilder().build(new File(file));
  }

  @Test
  public void testDeleteAgeGreaterThanThirty() throws Exception {
    String file =
        "/Users/pier/Documents/unimi/verifica e convalida software/progetto_DbUnit/src/test/resources/expectedDataset.xml";
    IDataSet expds = new FlatXmlDataSetBuilder().build(new FileInputStream(file));
    ITable expectedTable = expds.getTable("Employee");

    Session session = HibernateSessionFactory.getSession();
    Transaction txn = session.beginTransaction();
    employeeDao = new EmployeeDaoImpl(session);
    employeeDao.deleteAgeGreaterThanThirty();
    txn.commit();
    session.close();

    IDatabaseConnection connection = databaseTester.getConnection();
    IDataSet databaseDataSet = connection.createDataSet();
    ITable actualTable = databaseDataSet.getTable("Employee");
    Assertion.assertEquals(expectedTable, actualTable);
  }

  @AfterEach
  public void tearDown() throws Exception {
    databaseTester.setTearDownOperation(DatabaseOperation.NONE);
    // databaseTester.setTearDownOperation(DatabaseOperation.DELETE);
    databaseTester.onTearDown();
  }
}
