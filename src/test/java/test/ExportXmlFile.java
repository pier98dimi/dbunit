package test;

import java.io.FileOutputStream;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;

public class ExportXmlFile {
  private static final String JDBC_DRIVER = org.postgresql.Driver.class.getName();
  private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/postgres";
  private static final String USER = "postgres";
  private static final String PASSWORD = "";
  private static IDatabaseTester databaseTester;

  public static void main(String[] args) {
    try {
      databaseTester = new JdbcDatabaseTester(JDBC_DRIVER, JDBC_URL, USER, PASSWORD);
      IDatabaseConnection conn = null;
      conn = databaseTester.getConnection();
      QueryDataSet partDS = new QueryDataSet(conn);
      partDS.addTable("EMPLOYEE", "SELECT * FROM EMPLOYEE");
      FlatXmlDataSet.write(partDS, new FileOutputStream("employee.xml"));
    } catch (Exception e) {
      e.printStackTrace();
    }
    System.out.println("====DONE====");
  }
}
