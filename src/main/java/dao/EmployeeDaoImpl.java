package dao;

import entity.Employee;
import java.util.List;
import javax.persistence.TypedQuery;
import org.hibernate.Session;

public class EmployeeDaoImpl implements EmployeeDao {
  private Session session;

  public EmployeeDaoImpl(Session session) {
    this.session = session;
  }

  @Override
  @SuppressWarnings("unchecked")
  public List<Employee> getAllEmployees() {
    TypedQuery<Employee> query = (TypedQuery<Employee>) session.createQuery("FROM Employee");
    List<Employee> res = query.getResultList();
    return res;
  }

  @Override
  @SuppressWarnings("unchecked")
  public Long getAverageAge() {
    List<Employee> employees = getAllEmployees();
    return getAvgAge(employees);
  }

  private Long getAvgAge(List<Employee> employees) {
    Long sumOfAge = Long.valueOf(01);
    for (Employee employee : employees) {
      sumOfAge = sumOfAge + employee.getAge();
    }
    return sumOfAge / employees.size();
  }

  @Override
  public void deleteAgeGreaterThanThirty() {
    String query = "DELETE FROM Employee WHERE age > 30";
    session.createQuery(query).executeUpdate();
  }
}
