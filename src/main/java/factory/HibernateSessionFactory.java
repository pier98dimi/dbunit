package factory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateSessionFactory {
  private static org.hibernate.SessionFactory sessionFactory;

  private HibernateSessionFactory() {}

  public static Session getSession() {
    if (sessionFactory == null) {
      buildSessionFactory();
    }
    return (sessionFactory != null) ? sessionFactory.openSession() : null;
  }

  private static void buildSessionFactory() {
    StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    MetadataSources sources = new MetadataSources(registry);
    Metadata metadata = sources.getMetadataBuilder().build();
    sessionFactory = metadata.getSessionFactoryBuilder().build();
  }

  public static SessionFactory getSessionFactory() {
    return sessionFactory;
  }
}
